#!/usr/bin/python

import sys
import requests
import os.path

from PyQt5.QtCore import (pyqtSlot, QTimer, QDate, QTime)
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QDialog, QSystemTrayIcon)
from PyQt5.uic import loadUi


class Mullvad:
    " This is a class for checking Mullvad's API. "

    def __init__(self):
        pass

    def _get_response(self):
        " Get response from Mullvad's API "
        try:
            _r = requests.get('https://am.i.mullvad.net/json')
        except Exception:
            return None
        return _r

    def get_json(self):
        " Get response from Mullvad in JSON "
        _d = {}
        _r = self._get_response()
        if _r is not None:
            _r = _r.json()
            _d['ip'] = _r['ip']
            _d['country'] = _r['country']
            _d['city'] = _r['city']
            _d['mullvaded'] = _r['mullvad_exit_ip']
            _d['blacklisted'] = _r['blacklisted']['blacklisted']
            try:
                _d['hostname'] = _r['mullvad_exit_ip_hostname']
                _d['server_type'] = _r['mullvad_server_type']
            except Exception:
                _d['hostname'] = 'N/A'
                _d['server_type'] = 'N/A'
        else:
            _d['ip'] = 'Unknown'
            _d['country'] = ''
            _d['city'] = ''
            _d['hostname'] = ''
            _d['server_type'] = ''
            _d['mullvaded'] = False
            _d['blacklisted'] = False
        return _d


class MullvadApp(QDialog):
    def __init__(self, mullvad):
        super(MullvadApp, self).__init__()
        _ui_file = [
                    '.',
                    '/opt/mullvad-tray'
                  ]
        for self.mt_path in _ui_file:
            if os.path.isfile(self.mt_path + '/mullvad-tray.ui'):
                loadUi(self.mt_path + '/mullvad-tray.ui', self)
                break

        self.edit_ip.setReadOnly(True)
        self.edit_country.setReadOnly(True)
        self.edit_city.setReadOnly(True)
        self.edit_hostname.setReadOnly(True)
        self.edit_server_type.setReadOnly(True)
        self.checkbox_protected.setChecked(False)
        self.checkbox_blacklisted.setChecked(False)
        self.spinBox_interval.setValue(30)

        self.setWindowIcon(QIcon(self.mt_path + '/images/mullvad-tray-logo.png'))
        self.setWindowTitle('Mullvad Tray')
        self.tray = QSystemTrayIcon(self)
        self.tray.setIcon(self.windowIcon())
        self.tray.setToolTip("Mullvad Tray")
        self.tray.show()
        self.tray.setVisible(True)

        self.pushButton.clicked.connect(self.on_pushButton_clicked)
        # self.tray.activated.connect(lambda: self.hide() if self.isVisible() else self.show())
        self.tray.activated.connect(self._hide_or_show)

        self.interval = 3000
        self._status_update_timer = QTimer(self)
        self._status_update_timer.setSingleShot(False)
        self._status_update_timer.timeout.connect(self._update_status)
        self._status_update_timer.start(3000)

        self.mullvad = mullvad
        return

    @pyqtSlot()
    def on_pushButton_clicked(self):
        self.hide()
        self.exit()
        return

    @pyqtSlot()
    def _hide_or_show(self):
        if self.isVisible():
            self.oldpos = self.pos()
            self.hide()
        else:
            self.show()
            try:
                self.move(self.oldpos)
            except Exception:
                pass

    @pyqtSlot()
    def _update_status(self):
        self.now_date = QDate.currentDate()
        self.now_time = QTime.currentTime()

        self.label_last_check.setText(self.now_time.toString())
        self.label_last_check.setToolTip(f'{self.now_date.toString()} {self.now_time.toString()}')
        self.label_last_check_label.setToolTip(f'{self.now_date.toString()} {self.now_time.toString()}')

        self._j = j = self.mullvad.get_json()
        self.edit_ip.setText(j['ip'])
        self.edit_country.setText(j['country'])
        self.edit_city.setText(j['city'])
        self.edit_hostname.setText(j['hostname'])
        self.edit_server_type.setText(j['server_type'])
        self.checkbox_protected.setChecked(j['mullvaded'])
        self.checkbox_blacklisted.setChecked(j['blacklisted'])
        if j['ip'] is 'Unknown':
            self.tray.setIcon(QIcon(self.mt_path + '/images/mullvad-tray-logo.png'))
            self.tray.setToolTip("No Internet connection...")
        elif j['mullvaded'] and not j['blacklisted']:
            self.tray.setIcon(QIcon(self.mt_path + '/images/mullvad-tray-green.png'))
            self.tray.setToolTip(f'You are protected by Mullvad.\nIP: {j["ip"]}\nCountry: {j["country"]}\nCity: {j["city"]}')
        elif j['mullvaded'] and j['blacklisted']:
            self.tray.setIcon(QIcon(self.mt_path + '/images/mullvad-tray-yellow.png'))
            self.tray.setToolTip("You are protected by Mullvad\nbut IP is blacklisted.")
        else:
            self.tray.setIcon(QIcon(self.mt_path + '/images/mullvad-tray-red.png'))
            self.tray.setToolTip("You are NOT protected!\nConnect to Mullvad now.")
        self.setWindowIcon(self.tray.icon())
        if self.interval != self.spinBox_interval.value()*1000:
            self.interval = self.spinBox_interval.value()*1000
            self._status_update_timer.start(self.interval)
        return

    @property
    def exit(self):
        sys.exit()

    def closeEvent(self, event):
        if self.isVisible():
            self.hide()
        return

    def startEvent(self, event):
        if self.isVisible():
            self.hide()
        return

    def __icon_activated(self, reason):
        self.exit()
        return


app = QApplication(sys.argv)
widget = MullvadApp(Mullvad())
# widget.show()
sys.exit(app.exec_())
