# Mullvad Tray

## The repo is archived

I do not have time to further develop this project.
Feel free to fork it and play with it.

## What is Mullvad?

As you can read on [Mullvad.net](https://mullvad.net):

> Privacy is a universal right
>
> It is fundamental to a well-functioning society. It allows norms, ethics, and laws to be safely discussed and challenged. A free and open society, therefore, cannot flourish and develop nor exist without privacy.

You can think of Mullvad VPN as your first bastion to the Privacy on the Internet.

## Why does Mullvad Tray exist?

When you establish connection with Mullvad VPN using your router (pfsense, wrt, etc.) there is no other way to make sure you are protected than to visit [am.i.mullvad.net](https://am.i.mullvad.net) [^1].

Maybe you are like me, and you want to be sure you are, indeed, protected. This small widget creates a small system tray icon which will show you the status of the connection. It will also tell you what is your external IP and where is the exit node located.

## How does Mullvad Tray work?

It just makes the regular API calls to the Mullvad and returns the current connection status. Simple!

By default it checks your protection status every 30 seconds, but you can change the interval.

### Statuses

![protected](./images/green.png?raw=true) :: You are protected by Mullvad

![blacklisted](./images/yellow.png?raw=true) :: You are protected by Mullvad, but the IP is blacklisted.

![not-protected](./images/red.png?raw=true) :: You are not protected. Get Mullvad now!

### Main window

![window](./images/mullvad-tray.png)

## How to install Mullvad Tray?

Easy! Just clone this repository and run `python ./mullvad-tray.py`.

There is also an Arch Linux AUR package, so call:

```
$ pikaur -S mullvad-tray
```

[^1]: You can also call an Mullvad's API. Read more [here](https://am.i.mullvad.net/api)

## Donate

If you like this project, please donate. You can send coins to the following addresses.

**Bitcoin**: 3KWsKEw3Ewu7vcTREjQUuR8LUf4QXoZEHK

**Litecoin**: MHaqGAqMoQGiTkNyg7w8haC6mU25Frgi3M
